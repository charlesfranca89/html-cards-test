function init() {
    document.querySelectorAll('.card').forEach((item, index) => {
        item.addEventListener('click', event => {
            if (event.target.className.indexOf("close") > -1)
                return;
    
            // Deactivate current active item
            changeActiveCardStatus(false);
    
            // Activate item by index
            changeActiveCardStatus(true, index);
    
            // Change the list type to details
            changeListType("details")
        });
    });
    
    document.querySelectorAll('.close').forEach((item, index) => {
        item.addEventListener('click', event => {
            // Deactivate current active item
            changeActiveCardStatus(false);
    
            // Change the list type to list
            changeListType("list");
            event.stopPropagation();
        });
    });
}

function changeActiveCardStatus(active, index) {
    var activeElement = document.querySelector(".card.active");
    if (index >= 0)
        activeElement = document.querySelectorAll(".card")[index];

    if (!activeElement)
        return;

    if (active) {
        activeElement.className += " active";
    } else {
        activeElement.className = activeElement.className.replace(' active', '');
    }
}

function changeListType(type) {
    var cardsElement = document.querySelector(".cards");
    if (type == "details") {
        cardsElement.className = cardsElement.className.replace("list", "details");
    } else {
        cardsElement.className = cardsElement.className.replace("details", "list");
    }
}